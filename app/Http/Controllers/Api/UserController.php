<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use App\User;
use Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Post login api
     *
     * @bodyParam email string required
     * @bodyParam password string required
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $data = $request->all();
            $email = $data['email'];
            $password = $data['password'];
            $user = User::where('email', $email)->first();
            if ($user) {
                if (Hash::check($password, $user->_hashed_password)) {
                    Auth::login($user, true);
                    $success['user_id'] = $user->_id;
                    $success['username'] = $user->username;
                    $success['name'] = $user->name;
                    $success['email'] = $user->email;
                    //$success['token'] = $user->createToken('parsedb')->accessToken;
                    $body['status'] = 1;
                    $body['message'] = 'Success';
                    $body['data'] = $success;
                    return response()->json($body, 200);
                } else {
                    return response()->json(['status' => 0, 'message' => 'These credentials do not match our records1.'], 200);
                }
            } else {
                return response()->json(['status' => 0, 'message' => 'Invalid Email'], 200);
            }
        } catch (\Exception $e) {
            dd($e);
            $body['status'] = 0;
            $body['message'] = 'These credentials do not match our records2.';
            $body['data'] = null;
            return response()->json($body, 200);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 422);
            }
            $data = $request->all();
            $input['_hashed_password'] = bcrypt($data['password']);
            $input['email'] = $data['email'];
            $input['username'] = $data['email'];
            $input['name'] = $data['name'];
            $user = User::create($input);
            $success['user_id'] = $user->_id;
            $success['username'] = $user->username;
            $success['name'] = $user->name;
            $success['email'] = $user->email;
            //$success['token'] = $user->createToken('parsedb')->accessToken;
            $body['status'] = 1;
            $body['message'] = 'Success';
            $body['data'] = $success;
            return response()->json($body, 200);
        } catch (\Exception $e) {
            dd($e);
            $body['status'] = 0;
            $body['message'] = 'These credentials do not match our records2.';
            $body['data'] = null;
            return response()->json($body, 200);
        }
    }

    /**
     * Post logout api
     *
     * @bodyParam user_id int required
     * @return \Illuminate\Http\Response
     */
    public function logout_api(Request $request)
    {
        try {
            $data = $request->all();
            if (!isset($data['user_id']))
                return response()->json(['status' => 0, 'message' => 'Invalid request'], 200);
            auth('web')->logout();
            return response()->json(['status' => 1, 'message' => 'Successfully logout.'], 200);
        } catch (\Exception $e) {
            $body['status'] = 0;
            $body['message'] = 'Something went wrong, please try again.';
            return response()->json($body, 200);
        }
    }
}
